SHELL=cmd
CC = xc32-gcc
OBJCPY = xc32-bin2hex
ARCH = -mprocessor=32MX130F064B
OBJ = main.o
PORTN=$(shell type COMPORT.inc)

main.elf: $(OBJ)
	$(CC) $(ARCH) -o main.elf main.o -mips16 -DXPRJ_default=default -legacy-libc -Wl,-Map=main.map
	$(OBJCPY) main.elf
	@echo Success!
   
main.o: main.c
	$(CC) -mips16 -g -x c -c $(ARCH) -MMD -o main.o main.c -DXPRJ_default=default -legacy-libc

clean:
	@del *.o *.elf *.hex *.map *.d 2>NUL
	
LoadFlash:
	@Taskkill /IM putty.exe /F 2>NUL | wait 500
	pro32 -p main.hex
