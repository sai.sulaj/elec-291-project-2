#include <XC.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/attribs.h>
 
// Configuration Bits (somehow XC32 takes care of this)
#pragma config FNOSC = FRCPLL       // Internal Fast RC oscillator (8 MHz) w/ PLL
#pragma config FPLLIDIV = DIV_2     // Divide FRC before PLL (now 4 MHz)
#pragma config FPLLMUL = MUL_20     // PLL Multiply (now 80 MHz)
#pragma config FPLLODIV = DIV_2     // Divide After PLL (now 40 MHz)
#pragma config FWDTEN = OFF         // Watchdog Timer Disabled
#pragma config FPBDIV = DIV_1       // PBCLK = SYCLK
#pragma config FSOSCEN = OFF        // Secondary Oscillator Enable (Disabled)

// Defines
#define SYSCLK 40000000L
#define DEF_FREQ 8000L
#define Baud2BRG(desired_baud)( (SYSCLK / (16*desired_baud))-1)

float freq_min;
float freq_max;
float POSITIVE_THRESH_1 = 0.1;
float POSITIVE_THRESH_2 = 0.3;
float POSITIVE_THRESH_3 = 0.6;
float POSITIVE_THRESH_4 = 0.8;
int MOVING_AVERAGE_RANGE = 10;
int metal_detected = 0;

unsigned int read_pushbutton_no_debounce();
void set_led_1(unsigned int is_lit);
void set_led_2(unsigned int is_lit);
void set_led_3(unsigned int is_lit);
void set_led_4(unsigned int is_lit);
void toggle_led_1();
void toggle_led_2();
void toggle_led_3();
void toggle_led_4();
void toggle_speaker_polarity();
 
void UART2Configure(int baud_rate) {
	// Peripheral Pin Select
	U2RXRbits.U2RXR = 4;    //SET RX to RB8
	RPB9Rbits.RPB9R = 2;    //SET RB9 to TX

	U2MODE = 0;         // disable autobaud, TX and RX enabled only, 8N1, idle=HIGH
	U2STA = 0x1400;     // enable TX and RX
	U2BRG = Baud2BRG(baud_rate); // U2BRG = (FPb / (16*baud)) - 1

	U2MODESET = 0x8000;     // enable UART2
}

// Needed to by scanf() and gets()
int _mon_getc(int canblock) {
	char c;
	
	if (canblock) {
		while( !U2STAbits.URXDA); // wait (block) until data available in RX buffer
		c=U2RXREG;
		if(c=='\r') c='\n'; // When using PUTTY, pressing <Enter> sends '\r'.  Ctrl-J sends '\n'
		return (int)c;
	} else {
		// if data available in RX buffer
		if (U2STAbits.URXDA) {
			c=U2RXREG;
			if(c=='\r') c='\n';
			return (int)c;
		} else {
			return -1; // no characters to return
		}
	}
}

// Use the core timer to wait for 1 ms.
void wait_1ms(void) {
	unsigned int ui;
	_CP0_SET_COUNT(0); // resets the core timer count

	// get the core timer count
	while ( _CP0_GET_COUNT() < (SYSCLK/(2*1000)) );
}

void waitms(int len) {
	while(len--) wait_1ms();
}

#define PIN_PERIOD (PORTB&(1<<5))

// GetPeriod() seems to work fine for frequencies between 200Hz and 700kHz.
long int GetPeriod (int n) {
	int i;
	unsigned int saved_TCNT1a, saved_TCNT1b;
	
	_CP0_SET_COUNT(0); // resets the core timer count

	// Wait for square wave to be 0
	while (PIN_PERIOD!=0) {
		if(_CP0_GET_COUNT() > (SYSCLK/4)) return 0;
	}

	_CP0_SET_COUNT(0); // resets the core timer count

	// Wait for square wave to be 1
	while (PIN_PERIOD==0) {
		if(_CP0_GET_COUNT() > (SYSCLK/4)) return 0;
	}
	
	_CP0_SET_COUNT(0); // resets the core timer count

	// Measure the time of 'n' periods.
	for(i=0; i<n; i++) {
		// Wait for square wave to be 0.
		while (PIN_PERIOD!=0) {
			if(_CP0_GET_COUNT() > (SYSCLK/4)) return 0;
		}
		// Wait for square wave to be 1.
		while (PIN_PERIOD==0) {
			if(_CP0_GET_COUNT() > (SYSCLK/4)) return 0;
		}
	}

	return  _CP0_GET_COUNT();
}

void __ISR(_TIMER_1_VECTOR, IPL5SOFT) Timer1_Handler(void) {
	if (metal_detected) {
		toggle_speaker_polarity(); // Generate a 16kHz signal.
	}
	IFS0CLR=_IFS0_T1IF_MASK; // Clear timer 1 interrupt flag, bit 4 of IFS0
}

void SetupTimer1 (void) {
	// Explanation here:
	// https://www.youtube.com/watch?v=bu6TTZHnMPY
	__builtin_disable_interrupts();
	PR1 =(SYSCLK/(DEF_FREQ*2L))-1; // since SYSCLK/FREQ = PS*(PR1+1)
	TMR1 = 0;
	T1CONbits.TCKPS = 0; // Pre-scaler: 1
	T1CONbits.TCS = 0; // Clock source
	T1CONbits.ON = 1;
	IPC1bits.T1IP = 5;
	IPC1bits.T1IS = 0;
	IFS0bits.T1IF = 0;
	IEC0bits.T1IE = 1;
	
	INTCONbits.MVEC = 1; //Int multi-vector
	__builtin_enable_interrupts();
}

void update_isr1_freq(unsigned int freq) {
	__builtin_disable_interrupts();
	PR1 =(SYSCLK/(freq*2L))-1;
	TMR1 = 0;
	__builtin_enable_interrupts();
}

void process_freq(float freq) {
	float thresh_1 = freq_min + (freq_max - freq_min) * POSITIVE_THRESH_1;
	float thresh_2 = freq_min + (freq_max - freq_min) * POSITIVE_THRESH_2;
	float thresh_3 = freq_min + (freq_max - freq_min) * POSITIVE_THRESH_3;
	float thresh_4 = freq_min + (freq_max - freq_min) * POSITIVE_THRESH_4;
	unsigned int pb_pressed = read_pushbutton_no_debounce();
	long int next_freq;

	set_led_1(0);
	set_led_2(0);
	set_led_3(0);
	set_led_4(0);
	metal_detected = 0;

	if (freq > thresh_1) {
		metal_detected = 1;
		set_led_1(1);

		if (pb_pressed) {
			next_freq = 1319L;
		} else {
			next_freq = 659L;
		}
	}

	if (freq > thresh_2) {
		set_led_2(1);

		if (pb_pressed) {
			next_freq = 1175L;
		} else {
			next_freq = 740L;
		}
	}

	if (freq > thresh_3) {
		set_led_3(1);

		if (pb_pressed) {
			next_freq = 1047L;
		} else {
			next_freq = 784L;
		}
	}

	if (freq > thresh_4) {
		set_led_4(1);

		if (pb_pressed) {
			next_freq = 988L;
		} else {
			next_freq = 880L;
		}
	}

	update_isr1_freq(next_freq);
}

void set_led_1(unsigned int is_lit) {
	LATBbits.LATB3 = !is_lit;	
}
void set_led_2(unsigned int is_lit) {
	LATBbits.LATB4 = !is_lit;	
}
void set_led_3(unsigned int is_lit) {
	LATBbits.LATB1 = !is_lit;	
}
void set_led_4(unsigned int is_lit) {
	LATBbits.LATB0 = !is_lit;	
}
void toggle_led_1() {
	LATBbits.LATB3 = !LATBbits.LATB3;
}
void toggle_led_2() {
	LATBbits.LATB4 = !LATBbits.LATB4;
}
void toggle_led_3() {
	LATBbits.LATB1 = !LATBbits.LATB1;
}
void toggle_led_4() {
	LATBbits.LATB0 = !LATBbits.LATB0;
}
void toggle_speaker_polarity() {
	LATBbits.LATB6 = !LATBbits.LATB6;
}
unsigned int read_pushbutton() {
	if (!PORTBbits.RB2) {
		waitms(50);

		if (!PORTBbits.RB2) {
			while (!PORTBbits.RB2);
			return 1;
		}
	}

	return 0;
}

unsigned int read_pushbutton_no_debounce() {
	return !PORTBbits.RB2;
}

float read_freq() {
	long int count;
	float T, f = 0;
	int i;
	int average_count = 0;

	for (i = 0; i < MOVING_AVERAGE_RANGE; i++) {
		count=GetPeriod(100);

		if (count <= 0) {
			waitms(10);
			continue;
		}
		average_count++;

		T=(count*2.0)/(SYSCLK*100.0);
		f += 1/T;

		waitms(10);
	}

	if (average_count == 0) { return -1.0; }

	f /= average_count;

	return f;
}

void play_intro_sound() {
	metal_detected = 1;
	update_isr1_freq(659L);
	waitms(500);
	update_isr1_freq(740L);
	waitms(250);
	update_isr1_freq(784L);
	waitms(250);
	update_isr1_freq(880L);
	waitms(500);
	update_isr1_freq(587L);
	waitms(500);
	update_isr1_freq(659L);
	waitms(1000);
	metal_detected = 0;
}

void init_leds() {
	TRISBbits.TRISB0 = 0;
	TRISBbits.TRISB1 = 0;
	TRISBbits.TRISB3 = 0;
	TRISBbits.TRISB4 = 0;
	TRISBbits.TRISB6 = 0;
	LATBbits.LATB0 = 0;
	LATBbits.LATB1 = 0;
	LATBbits.LATB3 = 0;
	LATBbits.LATB4 = 0;
	LATBbits.LATB6 = 0;

	ANSELBbits.ANSB2 = 0;
	TRISBbits.TRISB2 = 1;
	CNPUBbits.CNPUB2 = 1;
}

void calibrate() {
	long int count;
	float reading;
	float progress;

	printf(
		"----- Calibration Mode -----\r\n"
		"Please keep metallic objects away from the inductor,\r\n"
		"and click the calibrate button to begin calibration.\r\n"
	);

	set_led_1(1);
	set_led_2(1);
	set_led_3(1);
	set_led_4(1);

	count = 0;
	while (!read_pushbutton()) {
		if (count > 20) {
			toggle_led_1();
			toggle_led_2();
			toggle_led_3();
			toggle_led_4();
			count = 0;
		}

		count += 1;
		waitms(20);
	}

	set_led_1(0);
	set_led_2(0);
	set_led_3(0);
	set_led_4(0);

	printf("Reading...\n");

	count = 0;
	reading = 0.0;
	while (count < 50) {
		reading += read_freq();
		count += 1;
		progress = (float) count * 2.0;

		if (progress > 25) {
			set_led_1(1);
		}
		if (progress > 50) {
			set_led_2(1);
		}
		if (progress > 75) {
			set_led_3(1);
		}

		if (count % 10 == 0) {
			printf("\rPercentage: %3.0f%                      ", progress);
		}

		waitms(30);
	}
	set_led_4(1);

	freq_min = reading / count;
	waitms(100);

	set_led_2(0);
	set_led_3(0);
	set_led_4(0);

	printf("\r\nDone! Minimum frequency: %f\r\n\n", freq_min);
	printf(
		"Please touch the inductor with a metallic object,\r\n"
		"click the calibrate button, and hold the metallic object there until prompted further.\r\n"
	);

	count = 0;
	while (!read_pushbutton()) {
		if (count > 20) {
			toggle_led_2();
			toggle_led_3();
			toggle_led_4();
			count = 0;
		}

		count += 1;
		waitms(20);
	}

	printf("Reading...\n");

	set_led_1(0);
	set_led_2(0);
	set_led_3(0);
	set_led_4(0);

	count = 0;
	reading = 0.0;
	while (count < 50) {
		reading += read_freq();
		count += 1;
		progress = (float) count * 2.0;

		if (progress > 25) {
			set_led_1(1);
		}
		if (progress > 50) {
			set_led_2(1);
		}
		if (progress > 75) {
			set_led_3(1);
		}

		if (count % 10 == 0) {
			printf("\rPercentage: %3.0f%                     ", (float) count * 2.0);
		}

		waitms(30);
	}

	freq_max = reading / count;
	printf("\r\n\nDone! Maximum frequency: %f\r\n\n", freq_max);

	waitms(150);

	freq_max = reading / count;
}

// Information here:
// http://umassamherstm5.org/tech-tutorials/pic32-tutorials/pic32mx220-tutorials/1-basic-digital-io-220
void main(void) {
	float freq;
	unsigned int cycle = 0;
	
	CFGCON = 0;
	SetupTimer1();
  
	UART2Configure(115200);  // Configure UART2 for a baud rate of 115200

	ANSELB &= ~(1<<5); // Set RB5 as a digital I/O
	TRISB |= (1<<5);   // configure pin RB5 as input
	CNPUB |= (1<<5);   // Enable pull-up resistor for RB5

	init_leds();
 
	play_intro_sound();

	printf("Welcome!\r\n");
	calibrate();

	while(1) {
		cycle += 1;

		if (cycle > 4) {
			cycle = 0;
			freq = read_freq();

			if (freq < 0) {
				printf(
					"NO SIGNAL                     \r\n"
					"                             \033[F"
				);
			} else {
				printf(
					"Frequency=%fHz                 \r\n"
					"Metal detected: %s        \033[F",
					freq, metal_detected ? "yes" : "no"
				);

				process_freq(freq);
			}
		}

		waitms(50);
        }
}
